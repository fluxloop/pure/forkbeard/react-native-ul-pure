package com.fluxloop2;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import org.json.JSONObject;

public class UlPureModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;

    public UlPureModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "UlPure";
    }

    @ReactMethod
    public void startTracking() {
    }

    @ReactMethod
    public void startTracking(Integer timeout) {
    }

    @ReactMethod
    public void setClientData(String userId, ReadableArray groups, String displayName, String status) {
    }

    @ReactMethod
    public void addOnZoneChangeListener() {

    }

    @ReactMethod
    public void addOnLocationChangeListener() {

    }

    @ReactMethod
    public void addOnLocationExitListener() {

    }

    @ReactMethod
    public void stopTracking() {

    }

    @ReactMethod
    public void simulateLocationChange(String locationId, Double latitude, Double longitude, Integer floor) {

    }

    private void sendEvent(String eventName, JSONObject jsonObject) {
        try {
            WritableMap map = null;
            if (jsonObject != null) {
                map = ReactNativeJson.convertJsonToMap(jsonObject);
            }

            getReactApplicationContext()
                    .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                    .emit(eventName, map);
        } catch (Exception e) {
            // fail silent
        }
    }
}
