# react-native-ul-pure

## Getting started

`$ npm install react-native-ul-pure@1.0.14 --save`

### Mostly automatic installation
#### Shared
1. `$ react-native link react-native-ul-pure`

#### Android
1. Insert the following lines inside the allprojects repositories block in `android/build.gradle`:
    ```groovy
    maven { url "https://dl.bintray.com/fluxloop/pure" }
    ```
2. Inside `android/build.gradle`, change the `compileSdkVersion` and `targetSdkVersion` to `29`.
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
    ```groovy
    implementation 'com.google.android.gms:play-services-location:17.0.0'
    ```
4. Add the following permissions inside the `application` block in `android/src/main/AndroidManifest.xml`:
    ```xml
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.RECORD_AUDIO" />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-permission android:name="android.permission.FOREGROUND_SERVICE" />
    <uses-permission android:name="android.permission.BLUETOOTH" />
    <uses-permission android:name="android.permission.BLUETOOTH_ADMIN" />
    ```
#### iOS
1. `$ cd ios && pod install`
2. Open your projects `.xcodeworkspace` in Xcode
3. In Xcode, in the project navigator, select your project.
4. Add `libreact-native-ul-pure.a` and `CoreLocation.framework` to your targets `Frameworks, Libraries, and Embedded Content`
5. Add `PureUL.framework` from `../node_modules/react-native-ul-pure/ios/Frameworks` to your targets `Frameworks, Libraries, and Embedded Content`
6. In `Build Settings` of your target, disable bitcode under `Build Options`
7. In `Build settings` of your target, add `$(PROJECT_DIR)/../node_modules/react-native-ul-pure/ios/Frameworks` as `recursive` to `Framework Search Paths`
8. Add these keys and values in your applications `Info.plist`:
```xml
    <key>NSMicrophoneUsageDescription</key>
    <string>For å fange opp ultralyd til å beregne din posisjon innendørs på steder med ultralyd posisjonering.</string>
    <key>NSBluetoothAlwaysUsageDescription</key>
    <string>For å kunne skru av mikrofonen når du ikke er på et sted med ultralyd posisjonering.</string>
    <key>NSLocationWhenInUseUsageDescription</key>
    <string>For å kunne skru av mikrofonen når du ikke er på et sted med ultralyd posisjonering.</string>
    <key>NSLocationAlwaysUsageDescription</key>
    <string>For å kunne skru av mikrofonen når du ikke er på et sted med ultralyd posisjonering.</string>
    <key>NSLocationAlwaysAndWhenInUseUsageDescription</key>
    <string>For å kunne skru av mikrofonen når du ikke er på et sted med ultralyd posisjonering.</string>
```


#### Manual installation
##### Android
1. Open up `android/app/src/main/java/[...]/MainApplication.java`
  - Add `import com.fluxloop2.UlPurePackage;` to the imports at the top of the file
  - Add `new UlPurePackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-ul-pure'
  	project(':react-native-ul-pure').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-ul-pure/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-ul-pure')
  	```
4. Follows steps 1 to 4 in mostly automatic installation > android




## Usage
**Import module:**
```javascript
import UlPure from 'react-native-ul-pure';
````
**Start tracking:**
```javascript
UlPure.setClientData("UserId", ["employees"], "DisplayName", "status");
UlPure.startTracking();
````

**Stop tracking:**
```javascript
UlPure.stopTracking();
```
**Subscribing to events:**
```javascript

// Android
import { DeviceEventEmitter } from 'react-native';
import UlPure from 'react-native-ul-pure';

componentDidMount() {
    UlPure.addOnLocationChangeListener();
    UlPure.addOnZoneChangeListener();
    UlPure.addOnLocationExitListener();

    this.locationChangeListener = DeviceEventEmitter.addListener("ULLocationChange", locationChange => console.log(JSON.stringify(locationChange)));
    this.zoneChangeListener = DeviceEventEmitter.addListener("ULZoneChange", zoneChange => console.log(JSON.stringify(zoneChange)));    
    this.locationExitListener = DeviceEventEmitter.addListener("ULLocationExit", () => { UlPure.stopTracking(); });    
}

// iOS
import { NativeEventEmitter } from 'react-native';
import UlPure from 'react-native-ul-pure';

const pureEmitter = new NativeEventEmitter(UlPure)
var locationChangeSubscription = null;
var zoneChangeSubscription = null;
var onExitSubscription = null;

componentDidMount() {
    UlPure.addOnLocationChangeListener();
    UlPure.addOnZoneChangeListener();
    UlPure.addOnLocationExitListener();

    locationChangeSubscription = pureEmitter.addListener("ULLocationChange", (locationChange) => console.log(JSON.stringify(locationChange)));
    zoneChangeSubscription = pureEmitter.addListener("ULZoneChange", (zoneChange) => console.log(JSON.stringify(zoneChange)));
    onExitSubscription = pureEmitter.addListener("ULLocationExit", () => { UlPure.stopTracking(); });    
}
```  

**Simulate location & zone change:**  
```javascript
import UlPure from 'react-native-ul-pure';

componentDidMount() {
  let locationId = "4bee5ffb-bbbb-46ba-ada2-1c828632a70c";
  let floor = 4;
  let latitude1 = 59.91368980471839;
  let longitude1 = 10.74141233434014
  let latitude2 = 59.91370626050136;
  let longitude2 = 10.741587015014176;

  UlPure.setClientData("UserId", ["employees"], "ReactNative", "available");
  UlPure.simulateLocationChange(locationId, latitude1, longitude1, floor);
  setTimeout(() => {
    UlPure.simulateLocationChange(locationId, latitude2, longitude2, floor);
  }, 2000);
}
```

**LocationChange emits:**
```json
{
    "appInstallationId": string,
    "groups": [string],
    "displayName": string,
    "status": string,
    "timestamp": double,
    "alt": float,
    "lat": double,
    "lng": double,
    "floor": int
}
```

**ZoneChange emits:**
```javascript
Zone:
{
    "id": string,
    "externalId": string,
    "tags": [string],
    "venue": {
        "id": string,
        "externalId": string,
        "tags": [string]
    }
}

ZoneChange:
{
    "current": {
        "zone": Zone,
        "duration": Long
    },
    "previous": {
        "zone": Zone,
        "duration": Long
    }
}
```


