//
//  PureUL.h
//  PureUL
//
//  Created by David Helland Wahl on 18/10/2019.
//  Copyright © 2019 David Helland Wahl. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for PureUL.
FOUNDATION_EXPORT double PureULVersionNumber;

//! Project version string for PureUL.
FOUNDATION_EXPORT const unsigned char PureULVersionString[];

#import "ULZoneDuration.h"
#import "ULZone.h"

@interface PureUL : NSObject

+ (instancetype _Nonnull)sharedInstance;

- (void)setClientData:(NSString*_Nonnull)userId groups:(NSArray*_Nonnull)groups displayName:(NSString*_Nonnull)displayName status:(NSString*_Nonnull)status;
- (void)start;
- (void)startWithTimeout:(int)timeout;
- (void)stop;
- (void)simulateLocation:(NSString*_Nonnull)locationId lat:(double)lat lng:(double)lng floor:(int)floor;
- (void)setInstallationId:(NSString*_Nonnull)installationId;
- (NSString*_Nonnull)getInstallationId;

@end
