//
//  ULVenue.h
//  PureUL
//
//  Created by David Helland Wahl on 21/10/2019.
//  Copyright © 2019 David Helland Wahl. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@interface ULVenue : NSObject<NSCopying>

@property (nonatomic,strong) NSString *id;
@property (nonatomic,strong) NSString *externalId;
@property (nonatomic,strong) NSMutableArray *tags;

-(id) initWithId:(NSString*)venueId externalId:(NSString*)externalId tags:(NSMutableArray*)tags;
-(id) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) toDictionary;
+(NSDictionary*) getDictionary:(ULVenue*)venue;


@end

NS_ASSUME_NONNULL_END
