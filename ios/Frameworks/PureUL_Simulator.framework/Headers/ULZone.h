//
//  ULZone.h
//  PureUL
//
//  Created by David Helland Wahl on 21/10/2019.
//  Copyright © 2019 David Helland Wahl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ULVenue.h"

NS_ASSUME_NONNULL_BEGIN

@interface ULZone : NSObject

@property (nonatomic,strong) NSString *id;
@property (nonatomic,strong) NSString *externalId;
@property (nonatomic,strong) NSMutableArray *tags;
@property (nonatomic,strong) ULVenue *venue;

-(id) initWith:(NSString*)id externalId:(nonnull NSString *)externalId tags:(nonnull NSMutableArray *)tags venue:(ULVenue*) venue;
-(id) initWithJsonData:(NSData*)jsonData;
-(NSDictionary*) toDictionary;
+(NSDictionary*) getDictionary:(ULZone*)zone;

@end

NS_ASSUME_NONNULL_END
