//
//  ULZoneDuration.h
//  PureUL
//
//  Created by David Helland Wahl on 21/10/2019.
//  Copyright © 2019 David Helland Wahl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ULZone.h"

NS_ASSUME_NONNULL_BEGIN

@interface ULZoneDuration : NSObject

@property (nonatomic,strong) ULZone *zone;
@property (assign) long duration;

-(id) initWithZone:(ULZone*)zone duration:(long)duration;
-(NSDictionary*) toDictionary;
+(NSDictionary*) getDictionary:(ULZoneDuration*)zone;

@end

NS_ASSUME_NONNULL_END
