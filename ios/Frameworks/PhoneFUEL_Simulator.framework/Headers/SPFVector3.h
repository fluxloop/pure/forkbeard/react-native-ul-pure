/**
 SPFVector3.h
 
 Copyright © 2017 Sonitor IPS Holding AS.
 Copyright © 2019 Forkbeard Technologies AS.
 All rights reserved.
 */
#import <UIKit/UIKit.h>

/**
 A class that contains a point in a three-dimensional coordinate system.
 */
@interface SPFVector3 : NSObject <NSCoding, NSSecureCoding>

#pragma mark Properties

/**
 The x-coordinate of the point.
 */
@property (nonatomic) CGFloat x;

/**
 The y-coordinate of the point.
 */
@property (nonatomic) CGFloat y;

/**
 The z-coordinate of the point.
 */
@property (nonatomic) CGFloat z;

-(instancetype) initWithX:(CGFloat)x;

-(instancetype) initWithX:(CGFloat)x y:(CGFloat)y;

-(instancetype) initWithX:(CGFloat)x y:(CGFloat)y z:(CGFloat)z;


#pragma mark Methods

/**
 Return True if all coordinates are NaN.
 
 @return True if all coordinates are NaN.
 */
-(BOOL) isNaN;

/**
 Return True if at least one coordinate is NaN.
 
 @return True if at least one coordinate is NaN.
 */
-(BOOL) hasNaN;

@end
