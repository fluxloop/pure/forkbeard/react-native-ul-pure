/**
 SPFInstallation.h
 
 Copyright © 2017 Sonitor IPS Holding AS.
 Copyright © 2019 Forkbeard Technologies AS.
 All rights reserved.
 */
#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

/**
 Represents installation information received from the Forbeard Cloud Service.
 */
@interface SPFInstallation : NSObject
@end
