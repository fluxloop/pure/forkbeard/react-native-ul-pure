/**
 SPFMapView.h
 
 Copyright © 2017 Sonitor IPS Holding AS.
 Copyright © 2019 Forkbeard Technologies AS.
 All rights reserved.
 */
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <GLKit/GLKit.h>

#import "SPFVector3.h"

@class SPFMapView;

/**
 Callback used for events originating from `SPFMapView`
 */
typedef void (^MapViewCallback)(SPFMapView* mapView);

/**
 A marker which can be used to render additional items on a `SPFMapView`.
 */
@interface SPFMapViewMarker : NSObject

/**
 Title to display alongside the marker
 */
@property (nonatomic, retain) NSString* title;

/**
 Color for the default marker visualisation.
 */
@property (nonatomic, retain) UIColor* color;

/**
 Image to display instead of the default marker visualisation.
 */
@property (nonatomic, retain) UIImage* image;

/**
 Size property for the image to display instead of the default marker visualisation.
 */
@property (nonatomic) CGSize imageSize;

/**
 Latitude for marker in degrees.
 */
@property (readonly) CGFloat latitude;

/**
 Longitude for marker in degrees.
 */
@property (readonly) CGFloat longitude;

/**
 Reference of the location where the marker should display relative to.
 Can be either the name or id of a location.
 */
@property (nonatomic, readonly) NSString* location;

/**
 Local coordinate relative to the the location defined by the given location.
 */
@property (nonatomic, readonly) SPFVector3* localCoordinate;

/**
 Create marker with the given geographic coordinates.
 These coordinates will be resolved when rendering the marker and using the active infrastructure.
 
 @param latitude
    Latitude to use for the marker
 @param longitude
    Longitude to use for the marker
 */
-(instancetype) initWithLatitude:(CGFloat)latitude longitude:(CGFloat)longitude;

/**
 Create marker with the given location. The marker will be rendered at the center of the location.
 The location will be resolved when rendering the marker and using the active infrastructure.
 
 If the location is not visible in the current infrastructure, the marker will not be rendered.
 
 @param location
    Location to use for the marker.
    This can either be the identifier of the location or the name.
 */
-(instancetype) initWithLocation:(NSString*)location;

/**
 Create marker with the given location and coordinates relative to that location.
 The location will be resolved when rendering the marker and using the active infrastructure.
 
 If the location is not visible in the current infrastructure, the marker will not be rendered.
 
 @param location
     Location to use for the marker.
     This can either be the identifier of the location or the name.
 @param localCoordinateX
    X coordinate relative to the given location.
 @param localCoordinateY
    Y coordinate relative to the given location.
 */
-(instancetype) initWithLocation:(NSString*)location localCoordinateX:(CGFloat)localCoordinateX localCoordinateY:(CGFloat)localCoordinateY;

/**
 Update the marker with the given geographic coordinates.
 These coordinates will be resolved when rendering the marker and using the active infrastructure.
 
 @param latitude
    Latitude to use for the marker
 @param longitude
    Longitude to use for the marker
 */
-(void) setLatitude:(CGFloat)latitude longitude:(CGFloat)longitude;

/**
 Update the marker with the given location and coordinates relative to that location.
 The location will be resolved when rendering the marker and using the active infrastructure.
 
 @param location
     Location to use for the marker.
     This can either be the identifier of the location or the name.
 @param localCoordinateX
    X coordinate relative to the given location.
 @param localCoordinateY
    Y coordinate relative to the given location.
 */
-(void) setLocation:(NSString*)location localCoordinateX:(CGFloat)localCoordinateX localCoordinateY:(CGFloat)localCoordinateY;

@end

/**
 Settings applicable to the rendering of the SPFMapView.
 
 @see `SPFMapView.renderSettings`
 */
IB_DESIGNABLE
@interface SPFMapViewRenderSettings : NSObject

/**
 Render text labels for location updates and markers.
 Defaults to true.
 */
@property (nonatomic, assign) IBInspectable BOOL renderTextLabels;

/**
 Render the outlines for all rendered locations.
 Defaults to true.
 */
@property (nonatomic, assign) IBInspectable BOOL renderLocationOutlines;

/**
 Render the overlays for all rendered locations.
 Defaults to true.
 */
@property (nonatomic, assign) IBInspectable BOOL renderLocationOverlays;

/**
 Render the estimated velocity at the current location for the device.
 Only available for locations with `SPFLocationResolution1D` resolution.
 
 @see `SPFLocation.velocity`, `SPFLocation.resolution`, `SPFLocationResolution`
 */
@property (nonatomic, assign) IBInspectable BOOL renderLocationUpdateVelocity;

/**
 Minimum horizontal velocity norm, in m/s, for rendering.
 
 @see `SPFLocation.velocity`, `SPFLocation.resolution`, `SPFLocationResolution`
 */
@property (nonatomic, assign) IBInspectable CGFloat minLocationUpdateVelocityToRender;

/**
 Render the estimated error for when the location update was calculated.
 @see `SPFLocation.errorEstimate`
 */
@property (nonatomic, assign) IBInspectable BOOL renderLocationUpdateError;

/**
 Render a line showing the history of the locations for x seconds.
 */
@property (nonatomic, assign) IBInspectable NSTimeInterval renderLocationUpdateHistory;

/**
 Highlight the current location for the device.
 */
@property (nonatomic, assign) IBInspectable BOOL highlightLocationUpdateLocation;

/**
 Hide location if it has `SPFLocationResolution0D` resolution.
 
 @see `SPFLocation.resolution`, `SPFLocationResolution`
 */
@property (nonatomic, assign) IBInspectable BOOL hideLocationUpdateResolution0D;

@end

/**
 A view which displays a map (with the model obtained from the Sense Server).
 
 Provides an easy graphical representation of the location map and location updates received from the positioning engine.
 The view automatically shows the map currently in use by the positioning engine.
 */
IB_DESIGNABLE
@interface SPFMapView : GLKView

#pragma mark Properties

/**
 Automatically focus on the latest received position for the current device.
 The view will try to automatically select the correct floor to render.
 */
@property (nonatomic, assign) IBInspectable BOOL followDevice;

/**
 Use simulated location updates.
 
 @see `SPFLocationManager.setTestLocationUpdatesEnabled`
 */
@property (nonatomic, assign) IBInspectable BOOL useTestLocationUpdates;

@property (nonatomic, retain) SPFMapViewRenderSettings* _Nonnull renderSettings;

/**
 Callback to indicate that the view has started rendering a new installation.
 */
@property (copy) MapViewCallback onMapViewLoadedCallback;

/**
 Callback to indicate that the view has stopped rendering the installation.
 */
@property (copy) MapViewCallback onMapViewUnloadedCallback;

/**
 List of floors available in the installation the view is currently rendering.
 These values can be used to select a floor.
 
 @see `setFloor:`
 */
@property (nonatomic, retain) NSArray<NSNumber*>* availableFloors;

#pragma mark Methods

/**
 Focus on the latest received position for the current device.
 The view will try to automatically select the correct floor to render.
 */
-(IBAction) focus;

/**
 Set the floor which should be rendered.
 
 @see `availableFloors`
 */
-(void) setFloor:(CGFloat)level;

/**
 Set the floor to render back to the default.
 The view will automatically try to select a most appropriate floor for the installation.
 */
-(void) unsetFloor;

/**
 Add a new marker to the view.
 
 @param marker
    The marker to add.
 
 @see `SPFMapViewMarker`
 */
-(void) addMarker:(SPFMapViewMarker*)marker;

/**
 Remove the given marker from the view.
 
 @param marker
    The marker to remove.
 
 @see `SPFMapViewMarker`
 */
-(void) removeMarker:(SPFMapViewMarker*)marker;

/**
 Remove all active markers from the view.
 */
-(void) clearMarkers;

@end
