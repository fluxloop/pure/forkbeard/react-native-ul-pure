/**
 SPFLocationManager.h
 
 Copyright © 2017 Sonitor IPS Holding AS.
 Copyright © 2019 Forkbeard Technologies AS.
 All rights reserved.
 */
#import <Foundation/Foundation.h>

#import "SPFState.h"

#import "SPFLocation.h"

#import "SPFInstallation.h"
#import "SPFLocationManagerDelegate.h"
#import "SPFLocationUpdateFilter.h"
#import "SPFTestLocation.h"

/**
 The SPFLocationManager is the starting point for using the positioning engine.
 You can use instances of this class to configure, start, and stop the Phone-FUEL positioning services.
 
 - SeeAlso: SPFLocationManagerDelegate
 */
@interface SPFLocationManager : NSObject

#pragma mark Properties

/**
 The delegate that will receive updates from the positioning engine.
 */
@property (nonatomic, weak) id<SPFLocationManagerDelegate> delegate;

/**
 The set of filters applied to the SPFLocation update stream.
 */
@property (nonatomic, retain) SPFLocationUpdateFilter * locationUpdateFilter;

#pragma mark Constructors

/**
 Create a new instance of the location manager.
 
 @return SPFLocationManager
 */
-(instancetype) init;

#pragma mark Methods

/**
 Check whether the Phone-FUEL positioning engine can be used on this device.

 @return Whether the Phone-FUEL positioning engine can be used on this device.
 */
-(BOOL) areLocationServicesAvailable;

/**
 Get the last known location for the current device.
 
 @return SPFLocation – The last known location for the current device.
 */
-(SPFLocation*) getLastKnownLocation;

/**
 Get the current state of the Phone-FUEL positioning engine.
 This is a snapshot of the state of the positioning engine at the time this method was called. The returned state will not be updated.

 @return Current state of the Phone-FUEL positioning engine.
 */
-(SPFState*) getState;

/**
 Starts the generation of updates that report the device's current location.
 This method will return instantly. Status(-errors) will be reported through spfLocationManager:stateChanged:withError: in the delegate.
 
 ! This will start the positioning engine if not yet started.
 */
-(void) startUpdatingLocation;

/**
 Starts the generation of updates that report the device's current location.
 This method will return instantly. Status(-errors) will be reported through spfLocationManager:stateChanged:withError: in the delegate.
 
 ! This will start the positioning engine if not yet started.
 
 @param completionHandler
                The completion handler, if provided, will be invoked after the positioning engine has been started or immediately when the positioning engine was already started.
 */
-(void) startUpdatingLocation:(void (^)(void))completionHandler;

/**
 Stops the generation of location updates.
 */
-(void) stopUpdatingLocation;

/**
 Stops the generation of location updates.
 
 @param completionHandler
                The completion handler, if provided, will be invoked after the positioning engine has been stopped or immediately when the positioning engine was not started.
 */
-(void) stopUpdatingLocation:(void (^)(void))completionHandler;

/**
 Check whether location updates have been started for this `SPFLocationManager`.
 */
-(BOOL) isUpdatingLocations;

/**
 Switch the use of simulated location updates.
 
 @param enabled
            Whether to enable to use of simulated location updates.
 
 @warning This affects all currently active location processes and can only be called when positioning is not active.
 
 -Throws:
    NSGenericException When this is called while location updates are already active.
 */
-(void) setTestLocationUpdatesEnabled:(BOOL)enabled;

/**
 Trigger a simulated location updates.
 
 @param location
            The location inside the test installation where the location update should take place.
 
 -Throws:
    NSGenericException When setTestLocationUpdatesEnabled has not been called with enabled:true.
    NSInvalidArgumentException When one or more of the given values are out of bounds. E.g. the coordinate lies outside the test location.
 */
-(void) setTestLocationUpdate:(SPFTestLocation)location;

/**
 Trigger a simulated location updates.
 
 @param location
            The location inside the test installation where the location update should take place.
 @param repeats
            The number of times the simulated location update should be repeated.
 @param intervalInSeconds
            Interval in seconds between the repeated location updates.
 
 -Throws:
    NSGenericException When setTestLocationUpdatesEnabled has not been called with enabled:true.
    NSInvalidArgumentException When one or more of the given values are out of bounds. E.g. the coordinate lies outside the test location.
 */
-(void) setTestLocationUpdate:(SPFTestLocation)location repeats:(NSUInteger)repeats intervalInSeconds:(NSTimeInterval)intervalInSeconds;

/**
 Trigger a simulated location updates.
 
 @param location
            The location inside the test installation where the location update should take place.
 @param repeats
            The number of times the simulated location update should be repeated.
 @param intervalInSeconds
            Interval in seconds between the repeated location updates.
 @param coordinate
            Coordinate for the simulated location update.
 
 -Throws:
    NSGenericException When setTestLocationUpdatesEnabled has not been called with enabled:true.
    NSInvalidArgumentException When one or more of the given values are out of bounds. E.g. the coordinate lies outside the test location.
 */
-(void) setTestLocationUpdate:(SPFTestLocation)location repeats:(NSUInteger)repeats intervalInSeconds:(NSTimeInterval)intervalInSeconds coordinate:(SPFVector3*)coordinate;

/**
 Trigger a simulated location updates.
 
 @param location
            The location inside the test installation where the location update should take place.
 @param repeats
            The number of times the simulated location update should be repeated.
 @param intervalInSeconds
            Interval in seconds between the repeated location updates.
 @param coordinate
            Coordinate for the simulated location update.
 @param velocity
            Velocity for the simulated location update.
 
 -Throws:
    NSGenericException When setTestLocationUpdatesEnabled has not been called with enabled:true.
    NSInvalidArgumentException When one or more of the given values are out of bounds. E.g. the coordinate lies outside the test location.
 */
-(void) setTestLocationUpdate:(SPFTestLocation)location repeats:(NSUInteger)repeats intervalInSeconds:(NSTimeInterval)intervalInSeconds coordinate:(SPFVector3*)coordinate velocity:(SPFVector3*)velocity;

/**
 Set the geographical coordinate for the anchor point used for the simulated location updates.
 By default this is set to the prime meridian/equator (0 degrees latitude, 0 degrees longitude).
 This will be used to calculate the geographic coordinate for the location update.
 
 @param latitude
             Latitude to set for the anchor point at the top-left corners of the building.
 @param longitude
             Longitude to set for the anchor point at the top-left corners of the building.
 */
-(void) setTestLocationUpdatesAnchorPointLatitude:(double)latitude longitude:(double)longitude;

@end
