/**
 SPFDiagnostics.h
 
 Copyright © 2019 Forkbeard Technologies AS.
 All rights reserved.
 */
#import <Foundation/Foundation.h>

/**
 This class allows the use of advanced diagnostics to help diagnose the cause of
 possible issues occurring during the development and/or deployment of applications using PhoneFUEL.
 
 @warning Use of this class can expose possible sensitive information and is only intended to be used for debugging purposes.
 */
@interface SPFDiagnostics : NSObject

#pragma mark Methods

+(void) enable;

+(void) disable;

@end
