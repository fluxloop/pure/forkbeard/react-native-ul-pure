/**
 SPFLocationManagerDelegate.h
 
 Copyright © 2017 Sonitor IPS Holding AS.
 Copyright © 2019 Forkbeard Technologies AS.
 All rights reserved.
 */
#import <Foundation/Foundation.h>

// forward declaration
@class SPFLocationManager;


/**
 - SeeAlso: SPFLocationManager
 
 ### Usage Objective-C: ###
 ```
 // CustomLocationManagerDelegate.h
 @interface CustomLocationManagerDelegate<SPFLocationManagerDelegate>
 @end
 
 
 // CustomLocationManagerDelegate.m
 @implementation CustomLocationManagerDelegate
 
 -(void) spfLocationManager:(SPFLocationManager*)manager didUpdateLocation:(SPFLocation*)location
 {
    NSLog(@"Received new location data.");
 }
 
 -(void) spfLocationManager:(SPFLocationManager*)manager stateChanged:(SPFState*)state withError:(NSError*)error
 {
    NSLog(@"State of positioning engine changed.");
 }
 
 @end
 ```
 
 ### Usage Swift: ###
 ```
 // CustomLocationManagerDelegate.swift
 class CustomLocationManagerDelegate: NSObject, SPFLocationManagerDelegate {
 
     func spfLocationManager(_ manager: SPFLocationManager!, didUpdate location: SPFLocation!) {
        NSLog("Received new location data.");
     }
 
 
     func spfLocationManager(_ manager: SPFLocationManager!, stateChanged state: SPFState!, withError error: Error! ) {
        NSLog("State of positioning engine changed.");
     }
 }
 ```
 */
@protocol SPFLocationManagerDelegate <NSObject>

#pragma mark Methods

@optional

/**
 Tells the delegate that new location data is available. Uhhh
 The rate at which this method is called can be influenced by setting `SPFLocationManager.interval` and/or `SPFLocationManager.distanceFilter`.
 
 @param manager
            The location manager object that forwarded the event.
 
 @param location
            The newly received location data.
 
 @see `SPFLocationManager.interval`, `SPFLocationManager.distanceFilter`
 */
-(void) spfLocationManager:(SPFLocationManager*)manager didUpdateLocation:(SPFLocation*)location;

/**
 Called when a new installation was found.
 
 @param manager
    The location manager object that forwarded the event.
 
 @param installation
    The installation that was found.
 */
-(void) spfLocationManager:(SPFLocationManager*)manager installationFound:(SPFInstallation*)installation;

/**
 Called when the positioning engine has started using a new installation.
 
 @param manager
    The location manager object that forwarded the event.
 
 @param installation
    The installation that was changed to.
 */
-(void) spfLocationManager:(SPFLocationManager*)manager installationChanged:(SPFInstallation*)installation;

/**
 Called when the status of the positioning engine has changed.
 
 @param manager
            The location manager object that forwarded the state.
 
 @param state
            The snapshot of the current state of the Phone-FUEL positioning engine.
            This is a snapshot of the state of the positioning engine at the time this method was called. The given state object will not be updated.
            For retrieving the most recent state `-[SPFLocationManager getState]` can be used.
 
 @param error
            If this is set an error has occurred. This is not necessarily a breaking error.
            When a SPFState has been received and this parameter is set the remaining fields need to be checked to see if the positioning engine is still active.
 */
-(void) spfLocationManager:(SPFLocationManager*)manager stateChanged:(SPFState*)state withError:(NSError*)error;

@end
