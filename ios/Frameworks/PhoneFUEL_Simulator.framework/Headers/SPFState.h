/**
 SPFState.h
 
 Copyright © 2017 Sonitor IPS Holding AS.
 Copyright © 2019 Forkbeard Technologies AS.
 All rights reserved.
 */
#import <Foundation/Foundation.h>

/**
 Describes state of the Phone-FUEL positioning engine.
 Every `SPFState` object represents a snapshot of the state and will not be updated.
 */
@interface SPFState : NSObject

#pragma mark Methods

/**
 Check if the positioning engine has been started.
 
 @return Whether the positioning engine has been started.
 */
-(BOOL) isStarted;

/**
 Check if the positioning engine is active and running.
 Locations for the device should be received.
 
 @return Whether the positioning engine is active.
 */
-(BOOL) isPositioning;

/**
 Check if the positioning engine is surveying for installations.
 
 @return Whether the positioning engine is surveying for installations.
 */
-(BOOL) isSurveying;

/**
 Check if the positioning engine is paused.
 This can happen when the microphone input is interrupted or when running in the background is disabled.
 
 @return Whether the positioning engine is paused.
 */
-(BOOL) isPaused;

@end
