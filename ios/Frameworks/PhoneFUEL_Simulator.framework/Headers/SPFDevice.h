/**
 SPFDevice.h
 
 Copyright © 2017 Sonitor IPS Holding AS.
 Copyright © 2019 Forkbeard Technologies AS.
 All rights reserved.
 */
#import <Foundation/Foundation.h>

/**
 Represents information about a device.
 This is not necessarily the current device.
 */
@interface SPFDevice : NSObject <NSCoding, NSSecureCoding>

#pragma mark Properties

/**
 UUID of the device.
 This UUID is unique per app-installation.
 */
@property (readonly, retain) NSString* uuid;

@end
