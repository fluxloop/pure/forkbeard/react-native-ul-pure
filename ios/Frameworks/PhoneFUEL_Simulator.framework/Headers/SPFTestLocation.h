/**
 SPFTestLocation.h
 
 Copyright © 2018 Sonitor IPS Holding AS.
 Copyright © 2019 Forkbeard Technologies AS.
 All rights reserved.
 */
#import <UIKit/UIKit.h>

/**
 Value representing location inside the simulated installation used for simulated location updates.
 Please review the documentation at https://portal.forkbeard.cloud/hc/en-gb/articles/360012187333 for more details on the infrastructure.
 
 -------------
 |     |     |
 |     |     |
 |     |     |
 -------------
 |           |
 -------------
 |           |
 |           |
 |           |
 -------------
 */
typedef NS_ENUM(NSUInteger, SPFTestLocation)
{
    /**
     Top-left room in the simulated installation.
     0D resolution.
     
     -------------
     |     |     |
     |  X  |     |
     |     |     |
     -------------
     |           |
     -------------
     |           |
     |           |
     |           |
     -------------
     
     @see `SPFLocationResolution0D`
     */
    SPFTestLocationRoom_1,
    
    /**
     Top-right room in the simulated installation.
     0D resolution.
     
     -------------
     |     |     |
     |     |  X  |
     |     |     |
     -------------
     |           |
     -------------
     |           |
     |           |
     |           |
     -------------
     
     @see `SPFLocationResolution0D`
     */
    SPFTestLocationRoom_2,
    
    /**
     Bottom area in the simulated installation.
     2D resolution.
     
     -------------
     |     |     |
     |     |     |
     |     |     |
     -------------
     |           |
     -------------
     | X X X X X |
     | X X X X X |
     | X X X X X |
     -------------
     
     @see `SPFLocationResolution2D`
     */
    SPFTestLocationArea,
    
    /**
     Center corridor in the simulated installation.
     1D resolution.
     
     -------------
     |     |     |
     |     |     |
     |     |     |
     -------------
     | X X X X X |
     -------------
     |           |
     |           |
     |           |
     -------------
     
     @see `SPFLocationResolution1D`
     */
    SPFTestLocationCorridor
};

