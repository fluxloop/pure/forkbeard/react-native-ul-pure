/**
 SPFLocationService.h
 
 Copyright © 2017 Sonitor IPS Holding AS.
 Copyright © 2019 Forkbeard Technologies AS.
 All rights reserved.
 */
#import <Foundation/Foundation.h>

/**
 Protocol for authentication events.
 */
@protocol SPFLocationServiceAuthenticationDelegate <NSObject>

/**
 Called when the API needs a token to communicate with the server.
 The token needs to be a standard JWT token (RFC 7519) that can be verified by the server.
 
 @return JWT token
 */
-(NSString*) onGetToken;

@end

/**
 Service that handles running the positioning engine.
 */
@interface SPFLocationService : NSObject

/**
 Set the name for the application/device. This can be used to identify the device on the Sense Server.
 The name is updated on the Sense Server after positioning has been (re)started and a connection to the Forkbeard Cloud Service has been made.

 @param name
    The name for the application/device.
    The name has a maximum length of 256 characters and is restricted to a specific character-set [a-Z, 0-9, `, \, ", -, +, _, ., $, @]
 
 - Throws:
    NSException if any of the above limitations are exceeded.
 */
+(void) setName:(NSString*)name;

/**
 Get the name for the application/device set via `+[SPFLocationService setName:name]`. This can be used to identify the device on the Sense Server.
 
 @return The name for the application/device set via `+[SPFLocationService setName:name]`.
 */

+(NSString*) getName;

/**
 Get the id of the installation to connect to.
 If not set, Phone-FUEL will try to automatically resolve this using the location information available.
 
 @return The id of the installation to connect to.
 */
+(NSString*) getInstallationId;

/**
 Manually set id of the installation to connect to.
 If not set, Phone-FUEL will try to automatically resolve this using the location information available.
 
 @param installationId
    The id of the installation to connect to.
    The id has a maximum length of 256 characters and is restricted to alphanumeric characters.
 
 - Throws:
    NSException if any of the above limitations are exceeded.
 */
+(void) setInstallationId:(NSString*)installationId;

/**
 Enable/disable running the Phone-FUEL positioning engine in the background. This changes this value for the entire application.
 
 - Requires:
    Requires 'audio' to be added to UIBackgroundModes in Info.plist
 
 @note Running in the background is enabled per-default when supported.
 
 - Throws:
    NSException if running in the background is not supported.
 */
+(void) setRunInBackground:(BOOL)runInBackground;

/**
 Delegate for authentication events. This delegate will be called whenever authentication information is required.
 */
+(void) setAuthenticationDelegate:(id<SPFLocationServiceAuthenticationDelegate>)authenticationDelegate;

@end
