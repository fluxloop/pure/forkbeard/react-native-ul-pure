/**
 SPFLocationUpdateFilter.h
 
 Copyright © 2019 Forkbeard Technologies AS.
 All rights reserved.
 */
#import <Foundation/Foundation.h>

#import "SPFLocation.h"

/**
 */
@interface SPFLocationUpdateFilter : NSObject

#pragma mark Properties
/**
 The desired interval for location updates, in seconds.
 
 This interval is inexact. The elapsed time between location updates will never be less than this interval.
 You may receive them slower than requested depending on how fast location updates can be detected.
 
 Default value is 0.
 */
@property (nonatomic) double interval;

/**
 Minimum distance, in meters, the device must move before an update event is generated.
 
 Default value is 0.
 */
@property (nonatomic) double distanceFilter;

/**
 If enabled, only location updates with non-nil transition attributes are propagated.
 
 Default value is false (disabled).
 */
@property (nonatomic) BOOL onlyOnTransition;

/**
 If enabled, all filters enabled are combined in disjunction (OR operator).
 If disabled, all filters enabled are combined in conjunction (AND operator).
 
 Default value is false (disabled).
 */
@property (nonatomic) BOOL filtersCombinedInDisjunction;

#pragma mark Constructors

/**
 Create a new instance of SPFLocationUpdateFilter.
 
 @return SPFLocationUpdateFilter
 */
-(instancetype) init;

#pragma mark Methods

/**
 */
-(BOOL) propagateLocationChanged:(SPFLocation*)location previousLocationUpdate:(SPFLocation*)previousLocationUpdate;

@end
