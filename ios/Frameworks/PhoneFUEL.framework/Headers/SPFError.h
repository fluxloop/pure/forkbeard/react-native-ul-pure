/**
 SPFError.h
 
 Copyright © 2017 Sonitor IPS Holding AS.
 Copyright © 2019 Forkbeard Technologies AS.
 All rights reserved.
 */
#import <Foundation/Foundation.h>

/**
 PhoneFUEL errors.
 */
extern NSErrorDomain const SPFErrorDomain;

/**
 PhoneFUEL-related Error Codes
 
 Constants used by NSError to indicate errors in the PhoneFUEL domain
 */
typedef NS_ERROR_ENUM(SPFErrorDomain, SPFErrorCodes)
{
    /**
     PhoneFUEL encountered an error that it can’t interpret.
     */
    SPFErrorUnknown                      = 343,
    
    /**
     The device is not authorized.
     */
    SPFErrorUnauthorized                 = 344,
    
    /**
     PhoneFUEL was unable to get required details from the Forkbeard Cloud Service.
     */
    SPFErrorLinksLookupFailed            = 345,
    
    /**
     Lookup of the proper infrastructure model has failed.
     */
    SPFErrorInfrastructureLookupFailed   = 346,
    
    /**
     An error was encountered while trying to parse the infrastructure model.
     */
    SPFErrorInfrastrucureParsingError    = 347,
    
    /**
     PhoneFUEL encountered an error while trying to access the audio input.
     */
    SPFErrorAudioInputError              = 348,
    
    /**
     There was an error trying to apply the correct configuration to the positioning engine
     */
    SPFErrorConfigurationerror           = 349,
    
    /**
     An error occurred while processing the audio input.
     */
    SPFErrorProcessingError              = 350,
    
    /**
     PhoneFUEL encountered an error when trying to run the positioning engine.
     */
    SPFErrorPositioningError             = 351,
    
    /**
     PhoneFUEL encountered an error when trying to communicate the the Forkbeard Cloud Service.
     */
    SPFErrorCloudServiceError            = 352,
    
    /**
     PhoneFUEL encountered an error when trying to access Bluetooth communication.
     @note Positioning will currently not work until this is resolved.
     */
    SPFErrorBluetoothError              = 353
};
