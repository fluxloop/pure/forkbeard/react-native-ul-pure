#import "UlPure.h"
#import "PureUL.h"


@implementation UlPure
{
    bool hasListeners;
}

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(requestBluetooth:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject){
    self.bleResolve = resolve;
    self.centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
}

RCT_EXPORT_METHOD(isBluetoothAvailable:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject){
    BOOL bleAvailable = false;
    if ( self.centralManager) {
        if (@available(iOS 10.0, *)) {
            bleAvailable = self.centralManager.state == CBManagerStatePoweredOn;
        }
    }
    resolve(@(bleAvailable));
}

RCT_EXPORT_METHOD(startTracking:(int)timeout) {
    [[PureUL sharedInstance] startWithTimeout:timeout];
}

RCT_EXPORT_METHOD(startTracking) {
    [[PureUL sharedInstance] start];
}

RCT_EXPORT_METHOD(stopTracking) {
    [[PureUL sharedInstance] stop];
    [self stopObserving];
}

RCT_EXPORT_METHOD(addOnZoneChangeListener) {
    [self startObserving];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(zoneChangeHandler:) name:@"ULZoneChange" object:nil];
}

RCT_EXPORT_METHOD(addOnLocationChangeListener) {
    [self startObserving];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationChangeHandler:) name:@"ULLocationChange" object:nil];
}

RCT_EXPORT_METHOD(addOnLocationExitListener) {
    [self startObserving];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationExitHandler:) name:@"ULLocationExit" object:nil];
}

RCT_EXPORT_METHOD(simulateLocationChange:(NSString *)locationId lat:(double)lat lng:(double)lng floor:(int)floor) {
   [[PureUL sharedInstance] simulateLocation:locationId lat:lat lng:lng floor:floor];
}

RCT_EXPORT_METHOD(setClientData:(NSString *)userId groups:(NSArray *)groups displayName:(NSString *)displayName status:(NSString *)status) {
   [[PureUL sharedInstance] setClientData:userId groups:groups displayName:displayName status:status];
}


- (NSArray<NSString *> *)supportedEvents {
    return @[@"ULZoneChange",@"ULLocationChange",@"BluetoothStateChange", @"ULLocationExit"];
}

-(void)centralManagerDidUpdateState:(CBCentralManager *)central {
    BOOL bleAvailable = false;
    if (@available(iOS 10.0, *)) {
        bleAvailable = self.centralManager.state == CBManagerStatePoweredOn;
    }
    if (self.bleResolve) {
        self.bleResolve(@(bleAvailable));
        self.bleResolve = nil;
    }
    [self sendEventWithName:@"BluetoothStateChange" body:@(bleAvailable)];
}

- (void)zoneChangeHandler:(NSNotification *)notification {
    if (hasListeners) {
        NSDictionary *zoneDataDictionary = [notification userInfo];
        [self sendEventWithName:@"ULZoneChange" body:zoneDataDictionary];
    }
}

- (void)locationChangeHandler:(NSNotification *)notification {
    if (hasListeners) {
        NSDictionary *locationChangeDictionary = [notification userInfo];
        [self sendEventWithName:@"ULLocationChange" body:locationChangeDictionary];
    }
}

- (void)locationExitHandler:(NSNotification *)notification {
    if (hasListeners) {
        [self sendEventWithName:@"ULLocationExit" body:@(YES)];
    }
}

-(void)startObserving {
    hasListeners = YES;
}

-(void)stopObserving {
    hasListeners = NO;
}
@end
