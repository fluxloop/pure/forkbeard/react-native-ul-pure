#import "React/RCTBridgeModule.h"
#import "React/RCTEventEmitter.h"
#import "PureUL.h"
#import <CoreBluetooth/CoreBluetooth.h>

@interface UlPure : RCTEventEmitter <RCTBridgeModule,CBCentralManagerDelegate>

@property (nonatomic, strong) CBCentralManager *centralManager;
@property (nonatomic, strong) RCTPromiseResolveBlock bleResolve;

@end
